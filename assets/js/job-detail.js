$(document).ready(function() {

  var hash = window.location.hash;
  if(hash.length > 1) {
    var uri = hash.substr(1);
    $.get(apiUrl + '/' + uri, function(data) {
      console.log(data.data);

      if(data.data != false) {
        var html = $("#job-details-container").html();
        html = prepareContent(html, data.data);
        $("#job-details-container").empty().append(html);
      } else {
        $("#job-details-container").empty().append("<h1 class='text-center'>Invalid job / Could not found details.</h1>");
      }

    });
  }

});



