var searchCntId = "searchCnt";
var searchCatId = "searchCat";
var searchBtnId = "searchBtn";
var jobResultHeaderId = "jobResult";

var resultContainerId = "resultContainer";
var resultTemplateIdPattern = "ResultTemplate";
var resultCountId = "resultCount";

var searchQuery = function() {
  var text = $("#" + searchCntId).val();
  return text.split(" ").join("+");
}

var searchCat = function() {
  var map = {"job": "job"};
  var def = "job";
  var category = $("#" + searchCatId).val();
  if (map.hasOwnProperty(category)) {
    return map[category];
  } else {
    return map[def];
  }
}

var getTemplate = function(cat) {
  return $("#" + cat + resultTemplateIdPattern).html();
}

var search = function() {

  var category = searchCat();
  var catUri = "/" + category;
  var searchTxt = $("#" + searchCntId).val();
  var queryStr = '?search=' + searchQuery();

  // Checking for validation
  if(!$("#" + searchCntId).get(0).checkValidity()) {
    return;
  }

  window.location.hash = "#"+category+"/"+searchText;
  $("#" + resultContainerId).empty().append("<div class='text-center'><i class='fa fa-spinner fa-3x fa-spin'></i></div>");

  $.get(apiUrl + catUri + queryStr, function(data) {

    console.log(data);
    if (data.hasOwnProperty('data')) {

      $("#" + resultContainerId).empty();
      $("#" + resultCountId).text(data.data.length);
      var template = getTemplate(category);
      for(var i in data.data) {
        var item = data.data[i];
        console.log(item);
        var content = prepareContent(template, item);
        $("#" + resultContainerId).append(content);
      }
      $('html, body').animate(
        { scrollTop: $("#" + jobResultHeaderId).offset().top }, 'slow', function () {
        // alert("reached top");
      });
    } else {
      $("#" + resultContainerId).empty().append("No result found !");
    }
  });

}

$(document).ready(function() {

  $("#" + searchBtnId).click(search);

  searchText = localStorage.getItem('searchCnt');
  if (searchText != null) {
    $("#" + searchCntId).val(searchText);
    search();
    // localStorage.removeItem("searchCnt");
  }

  $("#" + searchCntId).keypress(function(e) {
    if(e.which == 13) {
      search();
    }
  });

  $("#listViewBtn").click(function() {
    if(!$(this).hasClass('active')) {
      $("#" + resultContainerId).hide().addClass('list-view').fadeIn('slow');
      $('i.fa', $(this).parent()).removeClass('active');
      $(this).addClass('active');
    }
  });

  $("#gridViewBtn").click(function() {
    if(!$(this).hasClass('active')) {
      $("#" + resultContainerId).hide().removeClass('list-view').fadeIn('slow');
      $('i.fa', $(this).parent()).removeClass('active');
      $(this).addClass('active');
    }
  });

});