$('#cstm-owl-carousel1').owlCarousel({
	loop:true,
	margin:10,
	nav:false,
	// autoplay: true,
	// autoplayTimeout: 2000,
	responsive:{
			0:{
					items:1
			},
			600:{
					items:1
			},
			1000:{
					items:1
			}
	}
});

var owl = $('#cstm-owl-carousel1');
owl.owlCarousel();
$('.customNextBtn').click(function() {
	owl.trigger('next.owl.carousel');
});

$('.customPrevBtn').click(function() {
	owl.trigger('prev.owl.carousel', [300]);
})

$('#cstm-owl-carousel2').owlCarousel({
	loop:true,
	margin:10,
	nav:false,
	// autoplay: true,
	// autoplayTimeout: 2000,
	responsive:{
			0:{
					items:1
			},
			600:{
					items:1
			},
			1000:{
					items:3
			}
	}
});
var owl2 = $('#cstm-owl-carousel2');
owl2.owlCarousel();
$('.customNextBtn2').click(function() {
	owl2.trigger('next.owl.carousel');
})
$('.customPrevBtn2').click(function() {
	owl2.trigger('prev.owl.carousel', [300]);
})


$('#cstm-owl-carousel3').owlCarousel({
	loop:true,
	margin:10,
	nav:false,
	// autoplay: true,
	// autoplayTimeout: 2000,
	responsive:{
			0:{
					items:1
			},
			600:{
					items:1
			},
			1000:{
					items:4
			}
	}
});

var owl3 = $('#cstm-owl-carousel3');
owl3.owlCarousel();
$('.customNextBtn3').click(function() {
	owl3.trigger('next.owl.carousel');
})
$('.customPrevBtn3').click(function() {
	owl3.trigger('prev.owl.carousel', [300]);
});




$('#area-owl-carousel').owlCarousel({
	loop:true,
	margin:10,
	nav:false,
	// autoplay: true,
	// autoplayTimeout: 2000,
	responsive:{
		0:{
				items:1
		},
		600:{
				items:2
		},
		1000:{
				items:5
		}
	}
});
var owl_area = $('#area-owl-carousel');
owl_area.owlCarousel();
$('.areaOwlBtn_next').click(function() {
	owl_area.trigger('next.owl.carousel');
})
$('.areaOwlBtn_prev').click(function() {
	owl_area.trigger('prev.owl.carousel', [300]);
})



$('#client-owl-carousel').owlCarousel({
	loop:true,
	margin:10,
	nav:false,
	// autoplay: true,
	// autoplayTimeout: 2000,
	responsive:{
		0:{
				items:1
		},
		600:{
				items:2
		},
		1000:{
				items:5
		}
	}
});
var owl_client = $('#client-owl-carousel');
owl_client.owlCarousel();
$('.clientOwlBtn_next').click(function() {
	owl_client.trigger('next.owl.carousel');
})
$('.clientOwlBtn_prev').click(function() {
	owl_client.trigger('prev.owl.carousel', [300]);
})


