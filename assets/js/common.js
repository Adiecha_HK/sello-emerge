var apiUrl = 'http://api.leansystemsdev.co.za';

var prepareContent = function(template, data) {
  var keys = Object.keys(data)
  for(var i in keys) {
    var key = keys[i];
    template = template.split("{{"+key+"}}").join(data[key]);
  }
  return template
}